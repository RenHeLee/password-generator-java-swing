import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

public class PasswordGeneratorV2 {

	private JFrame frmPasswordGenerator;
	private JTextField generatedPasswordTextField;
	private JPasswordField passwordField;
	private JPasswordField passwordPasswordField;
	private JTextField purposeTextField;
	private JTextField usernameTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PasswordGeneratorV2 window = new PasswordGeneratorV2();
					window.frmPasswordGenerator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PasswordGeneratorV2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPasswordGenerator = new JFrame();
		frmPasswordGenerator.setResizable(false);
		frmPasswordGenerator.setAlwaysOnTop(true);
		frmPasswordGenerator.setTitle("Passowrd Generator 2.0");
		frmPasswordGenerator.setBounds(100, 100, 723, 356);
		frmPasswordGenerator.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblNewLabel, "2, 2, default, bottom");
		
		
		usernameTextField = new JTextField();
		usernameTextField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		usernameTextField.setColumns(10);
		frmPasswordGenerator.getContentPane().add(usernameTextField, "4, 2, 5, 1, fill, default");
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblPassword, "2, 4, default, bottom");
		
		passwordPasswordField = new JPasswordField();
		passwordPasswordField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(passwordPasswordField, "4, 4, 5, 1, fill, default");
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(passwordField, "8, 4, fill, default");
		
		JLabel lblPurpose = new JLabel("Purpose:");
		lblPurpose.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblPurpose, "2, 6, default, bottom");
		
		purposeTextField = new JTextField();
		purposeTextField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		purposeTextField.setColumns(10);
		frmPasswordGenerator.getContentPane().add(purposeTextField, "4, 6, 5, 1, fill, default");
		
		JLabel lblLength = new JLabel("Length:");
		lblLength.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblLength, "2, 8, default, bottom");
		
		JFormattedTextField lengthFormattedTextField = new JFormattedTextField();
		lengthFormattedTextField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lengthFormattedTextField, "4, 8, fill, default");
		
		JLabel lblCharacters = new JLabel("Characters:");
		lblCharacters.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblCharacters, "2, 10, default, bottom");
		
		JCheckBox uppercaseCheckBox = new JCheckBox("Uppercase");
		uppercaseCheckBox.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(uppercaseCheckBox, "4, 10, default, bottom");
		
		JCheckBox digitCheckBox = new JCheckBox("Digits");
		digitCheckBox.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(digitCheckBox, "6, 10, default, bottom");
		
		JCheckBox lowercaseCheckBox = new JCheckBox("Lowercase");
		lowercaseCheckBox.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lowercaseCheckBox, "4, 12, default, bottom");
		
		JCheckBox symbolCheckBox = new JCheckBox("Symbols");
		symbolCheckBox.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(symbolCheckBox, "6, 12, default, bottom");
		
		JButton generateButton = new JButton("Generate Password");
		generateButton.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		generateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = usernameTextField.getText();
                String password = passwordPasswordField.getText();
                String purpose = purposeTextField.getText();
                String lengthString = lengthFormattedTextField.getText();
                String generatedPassword = "";
                int length = Integer.parseInt(lengthString);
                String usernameShaValue = sha512(username);
                String passwordShaValue = sha512(password);
                String purposeShaValue = sha512(purpose);
                String key = sha512(String.valueOf(usernameShaValue) + passwordShaValue + purposeShaValue);
                String counterString = key.substring(0, 4);
                for (int counter = Integer.parseInt(counterString, 16), i = 0; i < counter; ++i) {
                    String temp = usernameShaValue;
                    usernameShaValue = passwordShaValue;
                    passwordShaValue = purposeShaValue;
                    purposeShaValue = key;
                    key = temp;
                    key = sha512(String.valueOf(usernameShaValue) + passwordShaValue + purposeShaValue + key);
                }
                String[] uppercase = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                String[] lowercase = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
                String[] digit = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                String[] symbol = { "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "-", "=", "[", "]", "{", "}", ";", ":", "<", ">", "?", ",", "." };
                ArrayList<String> charList = new ArrayList<String>();
                if (uppercaseCheckBox.isSelected()) {
                    charList.addAll(Arrays.asList(uppercase));
                }
                if (lowercaseCheckBox.isSelected()) {
                    charList.addAll(Arrays.asList(lowercase));
                }
                if (digitCheckBox.isSelected()) {
                    charList.addAll(Arrays.asList(digit));
                }
                if (symbolCheckBox.isSelected()) {
                    charList.addAll(Arrays.asList(symbol));
                }
                String[][] charTable = new String[16][16];
                int charAt = 0;
                for (int j = 0; j < 16; ++j) {
                    for (int k = 0; k < 16; ++k) {
                        charAt %= charList.size();
                        charTable[j][k] = charList.get(charAt);
                        ++charAt;
                    }
                }
                String[][] tempCharTable = new String[16][16];
                for (int counter = 0; counter < length; ++counter) {
                    char columnIndexChar = key.charAt(counter * 2 % 64);
                    char rowIndexChar = key.charAt((counter * 2 + 1) % 64);
                    int columnIndexInt = Integer.parseInt(Character.toString(columnIndexChar), 16);
                    int rowIndexInt = Integer.parseInt(Character.toString(rowIndexChar), 16);
                    generatedPassword = String.valueOf(generatedPassword) + charTable[columnIndexInt][rowIndexInt];
                }
                generatedPasswordTextField.setText(generatedPassword);
			}
		});
		frmPasswordGenerator.getContentPane().add(generateButton, "8, 12, default, bottom");
		
		JLabel lblYourPassword = new JLabel("Your Password:");
		lblYourPassword.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 20));
		frmPasswordGenerator.getContentPane().add(lblYourPassword, "2, 14, default, bottom");
		
		generatedPasswordTextField = new JTextField();
		generatedPasswordTextField.setFont(new Font("Lucida Sans Typewriter", Font.PLAIN, 30));
		generatedPasswordTextField.setColumns(10);
		frmPasswordGenerator.getContentPane().add(generatedPasswordTextField, "2, 16, 7, 1, fill, default");
	}

	public static String sha512(String input) {
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-512");
            final byte[] messageDigest = md.digest(input.getBytes());
            final BigInteger no = new BigInteger(1, messageDigest);
            String hashtext;
            for (hashtext = no.toString(16); hashtext.length() < 32; hashtext = "0" + hashtext) {}
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
